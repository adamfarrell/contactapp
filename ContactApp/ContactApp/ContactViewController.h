//
//  ContactViewController.h
//  ContactApp
//
//  Created by Adam Farrell on 7/11/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "AppDelegate.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "ViewController.h"
#import "UIColor+CustomColors.h"
#import "UIFont+CustomFonts.h"
#import "CustomButton.h"
#import "MapViewController.h"
#import "AddContactViewController.h"

@interface ContactViewController : UIViewController<MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate>
{
    AppDelegate* appDel;
    NSManagedObjectContext* context;
}
@property (nonatomic, strong)NSManagedObject* contact;
@end
