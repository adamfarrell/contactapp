//
//  AddContactViewController.m
//  ContactApp
//
//  Created by Adam Farrell on 7/11/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "AddContactViewController.h"

@interface AddContactViewController ()

@end

@implementation AddContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor primaryBackgroundColor];
    appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    context = [appDel managedObjectContext];
    float width = self.view.bounds.size.width;
//    float height = self.view.bounds.size.height;
    float xInset = 40;
//    [self registerForKeyboardNotifications];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(choosePicture:)];
    
    image = [[UIImageView alloc]initWithFrame:CGRectMake(xInset, 70, width - xInset * 2, 150)];
    image.image = [UIImage imageNamed:nil];
    [image setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:image];
    
    scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 230, width, 350)];

    
    name = [[UITextField alloc]initWithFrame:CGRectMake(xInset, 0, width - xInset * 2, 50)];
    name.placeholder = @"Full Name";
    name.inputAccessoryView = [self editingView];
    name.backgroundColor = [UIColor primaryButtonFontColor];
    [name setBorderStyle:UITextBorderStyleBezel];
//    [name setReturnKeyType:UIReturnKeyDone];
    [name setAutocorrectionType:UITextAutocorrectionTypeNo];
    [name setAutocapitalizationType:UITextAutocapitalizationTypeWords];
    name.delegate = self;
    name.tag = 1;
    [scrollView addSubview:name];
    
    phoneNum = [[UITextField alloc]initWithFrame:CGRectMake(xInset, 60, width - xInset * 2, 50)];
    phoneNum.placeholder = @"Phone Number";
    phoneNum.inputAccessoryView = [self editingView];
    phoneNum.backgroundColor = [UIColor primaryButtonFontColor];
    [phoneNum setBorderStyle:UITextBorderStyleBezel];
//    [phoneNum setReturnKeyType:UIReturnKeyDone];
    [phoneNum setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [phoneNum setAutocorrectionType:UITextAutocorrectionTypeNo];
    phoneNum.delegate = self;
    phoneNum.tag = 2;
    [scrollView addSubview:phoneNum];
    
    email = [[UITextField alloc]initWithFrame:CGRectMake(xInset, 120, width - xInset * 2, 50)];
    email.placeholder = @"Email";
    email.inputAccessoryView = [self editingView];
    email.backgroundColor = [UIColor primaryButtonFontColor];
    [email setBorderStyle:UITextBorderStyleBezel];
//    [email setReturnKeyType:UIReturnKeyDone];
    [email setAutocorrectionType:UITextAutocorrectionTypeNo];
    [email setKeyboardType:UIKeyboardTypeEmailAddress];
    [email setAutocorrectionType:UITextAutocorrectionTypeNo];
    email.delegate = self;
    email.tag = 3;
    [scrollView addSubview:email];
    
    address1 = [[UITextField alloc]initWithFrame:CGRectMake(xInset, 180, width - xInset * 2, 50)];
    address1.placeholder = @"Address Line 1";
    address1.inputAccessoryView = [self editingView];
    address1.backgroundColor = [UIColor primaryButtonFontColor];
    [address1 setBorderStyle:UITextBorderStyleBezel];
//    [address1 setReturnKeyType:UIReturnKeyDone];
    [address1 setAutocorrectionType:UITextAutocorrectionTypeNo];
    [address1 setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    address1.delegate = self;
    address1.tag = 4;
    [scrollView addSubview:address1];
    
    address2 = [[UITextField alloc]initWithFrame:CGRectMake(xInset, 240, width - xInset * 2, 50)];
    address2.placeholder = @"Address Line 2";
    address2.inputAccessoryView = [self editingView];
    address2.backgroundColor = [UIColor primaryButtonFontColor];
    [address2 setBorderStyle:UITextBorderStyleBezel];
//    [address2 setReturnKeyType:UIReturnKeyDone];
    [address2 setAutocorrectionType:UITextAutocorrectionTypeNo];
    [address2 setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    address2.delegate = self;
    address2.tag = 5;
    [scrollView addSubview:address2];
    
    city = [[UITextField alloc]initWithFrame:CGRectMake(xInset, 300, (width - xInset * 2) / 2 - 5, 50)];
    city.placeholder = @"City";
    city.inputAccessoryView = [self editingView];
    city.backgroundColor = [UIColor primaryButtonFontColor];
    [city setBorderStyle:UITextBorderStyleBezel];
//    [city setReturnKeyType:UIReturnKeyDone];
    [city setAutocorrectionType:UITextAutocorrectionTypeNo];
    [city setAutocapitalizationType:UITextAutocapitalizationTypeWords];
    city.delegate = self;
    city.tag = 6;
    [scrollView addSubview:city];
    
    state = [[UITextField alloc]initWithFrame:CGRectMake(xInset + ((width - xInset * 2) / 2), 300, (width - xInset * 2) / 2 - 5, 50)];
    state.placeholder = @"State";
    state.inputAccessoryView = [self editingView];
    state.backgroundColor = [UIColor primaryButtonFontColor];
    [state setBorderStyle:UITextBorderStyleBezel];
//    [state setReturnKeyType:UIReturnKeyDone];
    [state setAutocorrectionType:UITextAutocorrectionTypeNo];
    [state setAutocapitalizationType:UITextAutocapitalizationTypeWords];
    state.delegate = self;
    state.tag = 7;
    [scrollView addSubview:state];
    [self.view addSubview:scrollView];
    
    submit = [[CustomButton alloc]initWithFrame:CGRectMake(xInset, 590, width - xInset * 2, 50)];
    [submit setTitle:@"Submit" forState:UIControlStateNormal];
    [submit addTarget:self action:@selector(submitTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:submit];
    
    if (self.contact != nil) {
        image.image = [UIImage imageWithData:[self.contact valueForKey:@"persPicture"]];
        imageData = [self.contact valueForKey:@"persPicture"];
        name.text = [self.contact valueForKey:@"persName"];
        phoneNum.text = [self.contact valueForKey:@"persPhoneNum"];
        email.text = [self.contact valueForKey:@"persEmail"];
        NSArray* addressPieces = [[self.contact valueForKey:@"persAddress"] componentsSeparatedByString:@" "];
//        addressPieces[4] = [addressPieces[4] stringByReplacingOccurrencesOfString:@"," withString:@""];
        if (addressPieces.count == 6) {
            address1.text = [[NSString stringWithFormat:@"%@ %@ %@ %@", addressPieces[0], addressPieces[1], addressPieces[2], addressPieces[3]] stringByReplacingOccurrencesOfString:@"," withString:@""];
            city.text = addressPieces[4];
            state.text = addressPieces[5];
        } else if(addressPieces.count == 7) {
            address1.text = [NSString stringWithFormat:@"%@ %@ %@ %@", addressPieces[0], addressPieces[1], addressPieces[2], addressPieces[3]];
            address2.text = [addressPieces[4] stringByReplacingOccurrencesOfString:@"," withString:@""];
            city.text = addressPieces[5];
            state.text = addressPieces[6];
        }
    }
}

-(void)submitTouched:(id)sender {
    if (self.contact != nil) {
        [self.contact setValue:name.text forKey:@"persName"];
        [self.contact setValue:phoneNum.text forKey:@"persPhoneNum"];
        [self.contact setValue:email.text forKey:@"persEmail"];
        if (address2.text.length == 0) {
            [self.contact setValue:[NSString stringWithFormat:@"%@, %@ %@", address1.text, city.text, state.text] forKey:@"persAddress"];
        } else {
            [self.contact setValue:[NSString stringWithFormat:@"%@ %@, %@ %@", address1.text, [address2.text stringByReplacingOccurrencesOfString:@" " withString:@""], city.text, state.text] forKey:@"persAddress"];
        }
        [self.contact setValue:imageData forKey:@"persPicture"];
        NSError *error;
        BOOL saveSucceeded = [context save:&error];
        if (!saveSucceeded) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
        [self.navigationController popToRootViewControllerAnimated:YES];
    } else {
        person = [NSEntityDescription insertNewObjectForEntityForName:@"Person" inManagedObjectContext:context];
        [person setValue:name.text forKey:@"persName"];
        [person setValue:phoneNum.text forKey:@"persPhoneNum"];
        [person setValue:email.text forKey:@"persEmail"];
        if (address2.text.length == 0) {
            [person setValue:[NSString stringWithFormat:@"%@, %@ %@", address1.text, city.text, state.text] forKey:@"persAddress"];
        } else {
            [person setValue:[NSString stringWithFormat:@"%@ %@, %@ %@", address1.text, [address2.text stringByReplacingOccurrencesOfString:@" " withString:@""], city.text, state.text] forKey:@"persAddress"];
        }
        [person setValue:imageData forKey:@"persPicture"];
        NSError *error;
        BOOL saveSucceeded = [context save:&error];
        if (!saveSucceeded) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
//    NSLog([NSString stringWithFormat:@"%@ %@, %@ %@", address1.text, address2.text, city.text, state.text]);
}

//- (void)registerForKeyboardNotifications
//{
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWasShown:)
//                                                 name:UIKeyboardDidShowNotification object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillBeHidden:)
//                                                 name:UIKeyboardWillHideNotification object:nil];
//    
//}

//- (void)keyboardWasShown:(NSNotification*)aNotification {
//    NSDictionary* info = [aNotification userInfo];
//    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//    
//    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
//    scrollView.contentInset = contentInsets;
//    scrollView.scrollIndicatorInsets = contentInsets;
//    
//    // If active text field is hidden by keyboard, scroll it so it's visible
//    // Your app might not need or want this behavior.
//    CGRect aRect = self.view.frame;
//    aRect.size.height -= kbSize.height;
//    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
//        [scrollView scrollRectToVisible:activeField.frame animated:YES];
//    }
//}

//- (void)keyboardWillBeHidden:(NSNotification*)aNotification
//{
//    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
//    scrollView.contentInset = contentInsets;
//    scrollView.scrollIndicatorInsets = contentInsets;
//}

-(void)choosePicture:(id)sender {
    UIImagePickerController* picker = [[UIImagePickerController alloc]init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:^{
        
    }];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
    [scrollView setContentOffset:CGPointMake(0,textField.center.y-60) animated:YES];
}

//- (void)textFieldDidEndEditing:(UITextField *)textField
//{
//    activeField = nil;
//}

-(UIView*)editingView {
    UIView* editView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
    editView.backgroundColor = [UIColor primaryBackgroundColor];
    
    CustomButton* doneButton = [[CustomButton alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [doneButton addTarget:self action:@selector(doneTouched:) forControlEvents:UIControlEventTouchUpInside];
    [editView addSubview:doneButton];
    return editView;
}

-(void)doneTouched:(id)sender {
    [scrollView setContentOffset:CGPointMake(0,0) animated:YES];
    [self.view endEditing:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder *nextResponder = [textField.superview viewWithTag:nextTag];
    
    if (nextResponder) {
        [scrollView setContentOffset:CGPointMake(0,textField.center.y-60) animated:YES];
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        [scrollView setContentOffset:CGPointMake(0,0) animated:YES];
        [textField resignFirstResponder];
        return YES;
    }
    
    return NO;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [name resignFirstResponder];
    [address1 resignFirstResponder];
    [address2 resignFirstResponder];
    [phoneNum resignFirstResponder];
    [email resignFirstResponder];
    [city resignFirstResponder];
    [state resignFirstResponder];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    //put code for store image
    [self dismissViewControllerAnimated:picker completion:^{
        
    }];
    image.image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    imageData = UIImageJPEGRepresentation(image.image, 0.8);
    //Store image data in core data
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
