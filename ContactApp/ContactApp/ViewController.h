//
//  ViewController.h
//  ContactApp
//
//  Created by Adam Farrell on 7/11/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "AddContactViewController.h"
#import "UIColor+CustomColors.h"
#import "UIFont+CustomFonts.h"
#import "CustomButton.h"
#import "ContactViewController.h"

@interface ViewController : UIViewController<UINavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate>
{
    AppDelegate* appDel;
    UITableView* contacts;
    NSArray* fetchedContacts;
    NSManagedObjectContext* context;
}


@end

