//
//  ContactViewController.m
//  ContactApp
//
//  Created by Adam Farrell on 7/11/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "ContactViewController.h"

@interface ContactViewController ()

@end

@implementation ContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor primaryBackgroundColor];
    appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    context = [appDel managedObjectContext];
    float width = self.view.bounds.size.width;
    //    float height = self.view.bounds.size.height;
    float xInset = 40;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editTouched:)];
    
    UIImageView* image = [[UIImageView alloc]initWithFrame:CGRectMake(xInset, 70, width - xInset * 2, 150)];
    image.image = [UIImage imageWithData:[self.contact valueForKey:@"persPicture"]];
    [image setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:image];
    
    UILabel* name = [[UILabel alloc]initWithFrame:CGRectMake(xInset, 230, width - xInset * 2, 50)];
    name.text = [self.contact valueForKey:@"persName"];
    name.textColor = [UIColor primaryFontColor];
    name.font = [UIFont systemFontOfSize:34];
    name.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:name];
    
    UILabel* phoneNum = [[UILabel alloc]initWithFrame:CGRectMake(xInset, 290, (width - xInset * 2) / 2 - 5, 32)];
    phoneNum.text = [self.contact valueForKey:@"persPhoneNum"];
    phoneNum.textColor = [UIColor primaryFontColor];
    phoneNum.font = [UIFont systemFontOfSize:16];
//    phoneNum.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:phoneNum];
    
    UIButton* call = [[UIButton alloc]initWithFrame:CGRectMake(xInset + (width - xInset * 2) / 2, 290, (width - xInset * 2) / 4 - 2.5, 32)];
    [call setImage:[UIImage imageNamed:@"Phone-32"] forState:UIControlStateNormal];
    [call.imageView setContentMode:UIViewContentModeScaleAspectFit];
//    call.imageView.bounds = call.bounds;
    [call addTarget:self action:@selector(callTouched:) forControlEvents:UIControlEventTouchUpInside];
//    call.backgroundColor = [UIColor primaryButtonColor];
    [self.view addSubview:call];
    
    UIButton* msg = [[UIButton alloc]initWithFrame:CGRectMake(xInset + 3 * (width - xInset * 2) / 4, 290, (width - xInset * 2) / 4 - 2.5, 32)];
    [msg.imageView setContentMode:UIViewContentModeScaleAspectFit];
//    msg.imageView.bounds = msg.bounds;
    [msg setImage:[UIImage imageNamed:@"Message-32"] forState:UIControlStateNormal];
    [msg addTarget:self action:@selector(msgTouched:) forControlEvents:UIControlEventTouchUpInside];
//    msg.backgroundColor = [UIColor primaryButtonColor];
    [self.view addSubview:msg];
    
    UILabel* email = [[UILabel alloc]initWithFrame:CGRectMake(xInset, 340, 3 * (width - xInset * 2) / 4 - 5, 32)];
    email.text = [self.contact valueForKey:@"persEmail"];
    email.textColor = [UIColor primaryFontColor];
    email.font = [UIFont systemFontOfSize:16];
//    email.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:email];
    
    UIButton* mail = [[UIButton alloc]initWithFrame:CGRectMake(xInset + 3 * (width - xInset * 2) / 4, 340, (width - xInset * 2) / 4 - 5, 32)];
    [mail.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [mail setImage:[UIImage imageNamed:@"Email-32"] forState:UIControlStateNormal];
    [mail addTarget:self action:@selector(mailTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:mail];
    
    UILabel* address = [[UILabel alloc]initWithFrame:CGRectMake(xInset, 390, 3 * (width - xInset * 2) / 4 - 5, 32)];
    address.text = [self.contact valueForKey:@"persAddress"];
    address.textColor = [UIColor primaryFontColor];
    address.font = [UIFont systemFontOfSize:16];
    [self.view addSubview:address];
    
    UIButton* map = [[UIButton alloc]initWithFrame:CGRectMake(xInset + 3 * (width - xInset * 2) / 4, 390, (width - xInset * 2) / 4 - 5, 32)];
    [map.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [map setImage:[UIImage imageNamed:@"Map Marker-32"] forState:UIControlStateNormal];
    [map addTarget:self action:@selector(mapTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:map];
    
//    UIImageView* call = [[UIImageView alloc]initWithFrame:CGRectMake(xInset + (width - xInset * 2), 290, (width - xInset * 4) - 2.5, 50)];
//    call.image = [UIImage imageNamed:@"Phone-50"];
//    [call setContentMode:UIViewContentModeScaleAspectFit];
//    [self.view addSubview:call];
}

-(void)editTouched:(id)sender {
    AddContactViewController* acvc = [AddContactViewController new];
    acvc.contact = self.contact;
    [self.navigationController pushViewController:acvc animated:YES];
}

-(void)callTouched:(id)sender {
    NSString *cleanedString = [[[self.contact valueForKey:@"persPhoneNum"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
    NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]];
    [[UIApplication sharedApplication] openURL:telURL];
}

-(void)msgTouched:(id)sender {
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText])
    {
        controller.body = @"SMS message here";
        controller.recipients = [NSArray arrayWithObjects:[self.contact valueForKey:@"persPhoneNum"], nil];
        controller.messageComposeDelegate = self;
        [self presentViewController:controller animated:YES completion:^{
            
        }];
    }

}

-(void)mailTouched:(id)sender {
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    [controller setSubject:@"My Subject"];
    [controller setMessageBody:@"Hello there." isHTML:NO];
    [controller setToRecipients:@[[self.contact valueForKey:@"persEmail"]]];
    [self presentViewController:controller animated:YES completion:^{
        
    }];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    [self dismissModalViewControllerAnimated:YES];
}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    [self dismissViewControllerAnimated:controller completion:^{
        
    }];
}

-(void)mapTouched:(id)sender {
    MapViewController* mvc = [MapViewController new];
    mvc.contactAddress = [self.contact valueForKey:@"persAddress"];
    [self.navigationController pushViewController:mvc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
