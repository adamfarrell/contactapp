//
//  ViewController.m
//  ContactApp
//
//  Created by Adam Farrell on 7/11/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    context = [appDel managedObjectContext];
    contacts = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    contacts.delegate = self;
    contacts.dataSource = self;
    contacts.backgroundColor = [UIColor primaryBackgroundColor];
    [self.view addSubview:contacts];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addTouched:)];
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadMyContacts];
}

-(void)addTouched:(id)sender {
    AddContactViewController* acvc = [AddContactViewController new];
    [self.navigationController pushViewController:acvc animated:YES];
}

-(void)loadMyContacts {
    NSFetchRequest* fetchReq = [[NSFetchRequest alloc]init];
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"Person" inManagedObjectContext:context];
    NSError* error;
    [fetchReq setEntity:entity];
    fetchedContacts = [context executeFetchRequest:fetchReq error:&error];
    for (NSManagedObject* info in fetchedContacts) {
        NSLog(@"%@", [info valueForKey:@"persAddress"]);
    }
    
    [contacts reloadData];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return fetchedContacts.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    NSManagedObject* info = fetchedContacts[indexPath.row];
    cell.textLabel.text = [info valueForKey:@"persName"];
    cell.textLabel.textColor = [UIColor primaryButtonFontColor];
    cell.backgroundColor = [UIColor primaryButtonColor];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [contacts deselectRowAtIndexPath:indexPath animated:YES];
    
    NSManagedObject* selectedContact = fetchedContacts[indexPath.row];
    ContactViewController* cvc = [ContactViewController new];
    cvc.contact = selectedContact;
    
    [self.navigationController pushViewController:cvc animated:YES];
}

@end
