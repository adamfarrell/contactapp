//
//  MapViewController.h
//  ContactApp
//
//  Created by Adam Farrell on 7/13/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "CustomButton.h"
#import "UIColor+CustomColors.h"

@interface MapViewController : UIViewController<MKMapViewDelegate, CLLocationManagerDelegate>
@property (nonatomic, retain)CLLocationManager* locationManager;
@property (nonatomic, strong)NSString* contactAddress;
@end
