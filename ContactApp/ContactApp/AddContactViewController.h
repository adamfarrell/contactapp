//
//  AddContactViewController.h
//  ContactApp
//
//  Created by Adam Farrell on 7/11/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "UIColor+CustomColors.h"
#import "UIFont+CustomFonts.h"
#import "CustomButton.h"
#import "ContactViewController.h"

@interface AddContactViewController : UIViewController<UIImagePickerControllerDelegate, UITextFieldDelegate>
{
    AppDelegate* appDel;
    NSManagedObjectContext* context;
    NSManagedObject* person;
    UIImageView* image;
    NSData* imageData;
    UITextField* name;
    UITextField* phoneNum;
    UITextField* email;
    UITextField* address1;
    UITextField* address2;
    UITextField* city;
    UITextField* state;
    CustomButton* submit;
    UIScrollView* scrollView;
    UITextField* activeField;
}
@property (nonatomic, strong)NSManagedObject* contact;
@end
