//
//  MapViewController.m
//  ContactApp
//
//  Created by Adam Farrell on 7/13/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "MapViewController.h"

@interface MapViewController ()

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    MKMapView* map = [[MKMapView alloc]initWithFrame:self.view.bounds];
    map.delegate = self;
    //    map.showsUserLocation = YES;
    [self.view addSubview:map];
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:self.contactAddress completionHandler:^(NSArray* placemarks, NSError* error){
        for (CLPlacemark* aPlacemark in placemarks)
        {
            // Process the placemark.
            //            NSString *latDest1 = [NSString stringWithFormat:@"%.4f",aPlacemark.location.coordinate.latitude];
            //            NSString *lngDest1 = [NSString stringWithFormat:@"%.4f",aPlacemark.location.coordinate.longitude];
            MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
            [annotation setCoordinate:aPlacemark.location.coordinate];
            [annotation setTitle:@"WSU"]; //You can set the subtitle too
            [map addAnnotation:annotation];
            
            MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } };
            region.center.latitude = aPlacemark.location.coordinate.latitude ;
            region.center.longitude = aPlacemark.location.coordinate.longitude;
            region.span.longitudeDelta = 0.009f;
            region.span.latitudeDelta = 0.009f;
            [map setRegion:region animated:YES];
        }
    }];
    
    
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    // Use one or the other, not both. Depending on what you put in info.plist
    [self.locationManager requestWhenInUseAuthorization];
    //    [self.locationManager requestAlwaysAuthorization];
    
    [self.locationManager startUpdatingLocation];
    
    CustomButton* btn = [[CustomButton alloc]initWithFrame:CGRectMake(0, self.view.bounds.size.height - 50, self.view.bounds.size.width, 50)];
    [btn setTitle:@"Go To Maps" forState:UIControlStateNormal];
    btn.titleLabel.textAlignment = NSTextAlignmentCenter;
    btn.titleLabel.textColor = [UIColor primaryButtonFontColor];
    btn.backgroundColor = [UIColor primaryButtonColor];
    [btn addTarget:self action:@selector(btnTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];

}

-(void)btnTouched:(id)sender {
    NSString *addressString = [self.contactAddress stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    NSString* finalAddress = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@", addressString];
    NSURL *url = [NSURL URLWithString:finalAddress];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation {
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.005;
    span.longitudeDelta = 0.005;
    CLLocationCoordinate2D location;
    location.latitude = aUserLocation.coordinate.latitude;
    location.longitude = aUserLocation.coordinate.longitude;
    region.span = span;
    region.center = location;
    [aMapView setRegion:region animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
